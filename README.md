# Plate IQ - Backend Assignment

Create a web service with the following
requirements:
- API endpoints for the end customer 
- API endpoints for internal users / other microservices


### Running locally

Run following commend 
```bash
install and activate virtual environment
cd platiq
pip install -r requirements.txt
```

Run migrate command to initialize database
```
python manage.py migrate
```

Run Server 
```
python manage.py runserver
```

API is serviceable at `BASE_URL = "localhost:8000"`


