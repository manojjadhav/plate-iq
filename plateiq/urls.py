"""plateiq URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from rest_framework import routers
from rest_framework.authtoken import views

from apps.documents.views import DocumentViewset
from apps.invoices.views import InvoiceViewset
from apps.users.views import SignUpView


router = routers.SimpleRouter()
router.register("^api/v1/documents", DocumentViewset, basename="documents")
router.register("^api/v1/invoices", InvoiceViewset, basename="invoices")

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^api-token-auth/', views.obtain_auth_token),
    url(r"^api/v1/users/$", SignUpView.as_view(), name="self"),
]


urlpatterns += router.urls