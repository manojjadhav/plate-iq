from rest_framework import mixins, permissions, viewsets
from rest_framework.pagination import PageNumberPagination
from rest_framework.viewsets import ModelViewSet

DEFAULT_PERMISSION_CLASSES = (permissions.IsAuthenticated, )


class PlateIQPagination(PageNumberPagination):
    page_size = 1000
    page_size_query_param = 'page_size'
    max_page_size = 1000


class PlateIQPermissionMixin:
    permission_classes = DEFAULT_PERMISSION_CLASSES


class PlateIQUUIDMixin:
    lookup_field = 'pk'


# ==============================================================================
# Custom Plate IQ Viewsets from DRF Generic Viewsets - START
# for more info on viewset actions check link below
# - http://www.django-rest-framework.org/api-guide/viewsets/#viewset-actions
# ==============================================================================


class PlateIQModelViewSet(PlateIQPermissionMixin, ModelViewSet):
    '''
    Model view set with permissions, and setting uuid
    '''
    pass


class PlateIQReadOnlyModelViewSet(mixins.RetrieveModelMixin,
                               mixins.ListModelMixin, PlateIQPermissionMixin,
                               PlateIQUUIDMixin, viewsets.GenericViewSet):
    pass


class PlateIQReadWriteModelViewSet(mixins.RetrieveModelMixin,
                                mixins.ListModelMixin, mixins.UpdateModelMixin,
                                PlateIQPermissionMixin, PlateIQUUIDMixin,
                                viewsets.GenericViewSet):
    pass


class PlateIQWriteModelViewSet(mixins.UpdateModelMixin, mixins.CreateModelMixin,
                            PlateIQPermissionMixin, PlateIQUUIDMixin, viewsets.GenericViewSet):
    pass


class PlateIQReadWriteCreateModelViewSet(PlateIQPermissionMixin,
                                      mixins.RetrieveModelMixin, mixins.ListModelMixin,
                                      mixins.UpdateModelMixin, mixins.CreateModelMixin,
                                      PlateIQUUIDMixin, viewsets.GenericViewSet):
    pass

# ==============================================================================
# Custom PlateIQ DRF Viewsets - END
# ==============================================================================
