from django.db import models


class Abstract(models.Model):
    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


class IsActiveAbstractModel(Abstract):

    is_active = models.BooleanField(default=True)

    class Meta:
        abstract = True