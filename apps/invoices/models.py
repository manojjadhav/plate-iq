from django.db import models, transaction
from apps.base.models import IsActiveAbstractModel
from apps.documents.models import Documents
# Create your models here.


class InvoiceManager(models.Manager):

    def create_or_update_invoice(self, caller, user, name, descriptions, document_id, invoice_number, note, invoice_id=None):
        """
        Purpose: Create or update line items for given invoice

        """
        # TODO: Add line item and update invoice

        obj = None
        if invoice_id:
            obj = self.get(id=invoice_id)
            obj.name = name
            obj.description = descriptions
            obj.document_id = document_id
            obj.invoice_number = invoice_number

        else:
            obj = self.create(
                name=name,
                description=descriptions,
                document_id=document_id,
                invoice_number=invoice_number
            )
        return obj

    @transaction.atomic
    def create_invoice(self, caller, user, data):
        """
        Create invoice and invoice items
        """
        inv_obj = self.create_or_update_invoice(caller, user, data.get('name', "Testing"),
                                                data['description'],
                                                data['document_id'],
                                                data["invoice_number"],
                                                data.get("note", "")
                                                )
        # Proces invoice item
        for line_item in data['line_items']:
            InvoiceItems.objects.create_or_update_line_item(
                caller, user, inv_obj, line_item['quantity'],
                line_item['name'], line_item['description'],
                line_item['price'], line_item['discounted_price']
            )

        return inv_obj

    @transaction.atomic
    def update_invoice(self, caller, user, data, invoice_id):
        """
        Create invoice and invoice items
        """
        inv_obj = self.create_or_update_invoice(caller, user, data['name'],
                                                data['description'],
                                                data['document_id'],
                                                data["invoice_number"],
                                                data.get("note", ""),
                                                invoice_id
                                                )
        # Proces invoice item
        line_item_ids = []
        for line_item in data['line_items']:
            obj = InvoiceItems.objects.create_or_update_line_item(
                caller, user, inv_obj, line_item['quantity'],
                line_item['name'], line_item['description'],
                line_item['price'], line_item['discounted_price'],
                line_item.get('id')
            )
            line_item_ids.append(obj.id)

        # mark line item inactive which are not updated
        InvoiceItems.objects.filter(invoice=inv_obj).exclude(id__in=line_item_ids).update(is_active=False)
        return inv_obj


class Invoice(IsActiveAbstractModel):

    name = models.CharField(max_length=255, )

    description = models.TextField(null=True, blank=True)

    total_amount = models.FloatField(default=0.0)

    discounted_amount = models.FloatField(default=0.0)

    # link document
    document = models.ForeignKey(Documents, on_delete=models.CASCADE, related_name="invoice")

    # We can have custom logic here for - Prefix for per customer (INV, REC etc
    invoice_number = models.TextField(null=True, blank=True)

    note = models.TextField(null=True, blank=True)

    objects = InvoiceManager()

    @property
    def line_items(self):
        return self.invoice_items.filter(is_active=True)


class InvoiceItemsManager(models.Manager):

    def create_or_update_line_item(self, caller, user, invoice_obj, quantity,  line_item_name, line_item_description,
                                   price, discounted_price, line_item_id=None):
        """
        Purpose: Create or update line items for given invoice

        """
        obj = None
        if line_item_id:
            obj = self.get(id=line_item_id)
            obj.line_item_name = line_item_name
            obj.quantity = quantity
            obj.line_item_description = line_item_description
            obj.price = price
            obj.discounted_price = discounted_price
            obj.save()
        else:
            obj = self.create(
                invoice=invoice_obj,
                line_item_name=line_item_name,
                quantity=quantity,
                line_item_description=line_item_description,
                price=price,
                discounted_price=discounted_price,
            )
        return obj


class InvoiceItems(IsActiveAbstractModel):

    invoice = models.ForeignKey(
        Invoice, related_name="invoice_items", on_delete=models.CASCADE
    )

    line_item_name = models.CharField(max_length=255, )

    line_item_description = models.CharField(max_length=255, null=True, blank=True)

    price = models.FloatField(default=0.0)

    discounted_price = models.FloatField(default=0.0)

    quantity = models.FloatField(default=1.0)

    objects = InvoiceItemsManager()
