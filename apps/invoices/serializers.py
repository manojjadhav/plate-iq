from rest_framework import serializers
from apps.invoices.models import Invoice, InvoiceItems


class InvoiceSerializer(serializers.ModelSerializer):
    line_items = serializers.SerializerMethodField()

    class Meta:
        model = Invoice
        fields = ('id', "name", 'description', 'total_amount', "line_items", 'discounted_amount',
                  'invoice_number', "note")

        read_only_fields = ('id', "line_items")

    def get_line_items(self, instance):
        return InvoiceItemSerializer(instance.line_items, many=True).data



class InvoiceItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = InvoiceItems
        fields = ('id', 'quantity', 'line_item_name', 'line_item_description', 'price', 'discounted_price')
        read_only_fields = ('id', )