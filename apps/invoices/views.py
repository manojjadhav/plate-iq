from __future__ import unicode_literals

# django & DRF import
from django_filters import rest_framework as filters
from django.utils import timezone
from rest_framework.filters import SearchFilter
from rest_framework.response import Response
from rest_framework.views import APIView
from django.http import HttpResponse
from rest_framework import status
from rest_framework import mixins, permissions
from rest_framework.decorators import action

# app import
from apps.base.rest_api import PlateIQReadWriteCreateModelViewSet, PlateIQPagination
from apps.invoices.models import Invoice, InvoiceItems
from apps.invoices.serializers import InvoiceSerializer, InvoiceItemSerializer
from apps.invoices.custom_exc import handle_exceptions


class InvoiceViewset(PlateIQReadWriteCreateModelViewSet, mixins.DestroyModelMixin):
    permission_classes = (permissions.IsAdminUser,)
    pagination_class = PlateIQPagination
    serializer_class = InvoiceSerializer
    queryset = Invoice.objects.filter(is_active=True)

    filter_backends = (filters.DjangoFilterBackend, SearchFilter)

    @handle_exceptions()
    def create(self, request, *args, **kwargs):
        """ create invoice """
        obj = Invoice.objects.create_invoice(request.user, request.user, request.data)
        data = InvoiceSerializer(obj).data
        return Response(status=status.HTTP_201_CREATED, data=data)


    @handle_exceptions()
    def update(self, request, *args, **kwargs):
        data = request.data
        obj = Invoice.objects.update_invoice(request.user, request.user, request.data, kwargs['pk'])
        data = InvoiceSerializer(obj).data
        return Response(status=status.HTTP_200_OK, data=data)

    def destroy(self, request, *args, **kwargs):
        """ Soft delete the invoice"""
        obj = self.get_object()
        obj.is_active = False
        obj.save()
        return Response(status=status.HTTP_204_NO_CONTENT)






