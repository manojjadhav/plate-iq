from django.shortcuts import render
from rest_framework.views import APIView
from apps.users.serializers import UserSerializer
from rest_framework.response import Response
from django.http import HttpResponse
from rest_framework import status


class SignUpView(APIView):
    permission_classes = ()
    authentication_classes = ()

    def post(self, request, *args, **kwargs):
        """ Register user """
        data = request.data
        user_serializer = UserSerializer(data=data)
        user_serializer.is_valid(raise_exception=True)
        user = user_serializer.save()
        user.set_password(data.get("passsword"))
        user.save()
        return Response(status=status.HTTP_201_CREATED,)
