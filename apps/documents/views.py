from __future__ import unicode_literals

# django & DRF import
from django_filters import rest_framework as filters
from django.utils import timezone
from rest_framework.filters import SearchFilter
from rest_framework.response import Response
from rest_framework.views import APIView
from django.http import HttpResponse
from rest_framework import status
from rest_framework import mixins, permissions
from rest_framework.decorators import action

# app import
from apps.base.rest_api import PlateIQReadWriteCreateModelViewSet, PlateIQPagination
from apps.documents.models import Documents
from apps.documents.serializers import DocumentSerializer
from apps.documents.custom_exc import handle_exceptions



class DocumentViewset(PlateIQReadWriteCreateModelViewSet, mixins.DestroyModelMixin):
    pagination_class = PlateIQPagination
    serializer_class = DocumentSerializer
    queryset = Documents.objects.filter(is_active=True)

    filter_backends = (filters.DjangoFilterBackend, SearchFilter)

    @handle_exceptions()
    def create(self, request, *args, **kwargs):
        data = {
            'name': request.data['file'].name,
            "by_customer_id": request.user.id,
            "file": request.data['file'],
            "note": request.data.get("note", None)
        }
        file_serializer = DocumentSerializer(data=data)

        if file_serializer.is_valid():
            file_serializer.save()
            return Response(file_serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, *args, **kwargs):
        """ Perform soft delete"""

        obj_instance = self.get_object()
        obj_instance.is_active=False
        obj_instance.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

    # to get status of document
    @action(methods=["GET"], detail=True, )
    def status(self, request, *args, **kwargs):
        obj_instance = self.get_object()
        return Response(status=status.HTTP_200_OK, data={"status": obj_instance.digitized_status})

    # Todo: PErmission allowed only for staff or super user
    @action(methods=["POST"], detail=True, permission_classes=[permissions.IsAdminUser] )
    def mark_as_digitized(self, request, *args, **kwargs):
        obj_instance = self.get_object()
        obj_instance.status = Documents.COMPLETED
        obj_instance.save()
        return Response(status=status.HTTP_200_OK, data={"status": obj_instance.status})



