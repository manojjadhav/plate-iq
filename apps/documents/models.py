from django.db import models
from apps.base.models import IsActiveAbstractModel
from django.contrib.auth.models import User


class Documents(IsActiveAbstractModel):
    IN_PROGRESS = 1
    COMPLETED = 2
    CANCELLED = 3

    STATUS_COICES = (
        (IN_PROGRESS, "In Progress"),
        (COMPLETED, "Completed"),
        (CANCELLED, "Cancelled")
    )

    # name
    name = models.CharField(max_length=255,)

    # uploaded by_customer
    by_customer = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)

    # s3 url can use later
    file = models.FileField(blank=False, null=False)

    # status
    digitized_status = models.SmallIntegerField(choices=STATUS_COICES, default=IN_PROGRESS)

    # Note if failed or success
    note = models.TextField(null=True, blank=True)






