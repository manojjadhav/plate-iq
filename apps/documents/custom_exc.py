# __author__ : Manoj Jadhav
from functools import wraps
from rest_framework.response import Response
from rest_framework import status
from django.core.exceptions import ValidationError

def handle_exceptions():
    """ Handle exception for documents"""

    def wrapper(fn):
        # noinspection PyBroadException
        @wraps(fn)
        def decorator(*args, **kwargs):
            try:
                return fn(*args, **kwargs)
            except KeyError as exc:
                return Response(status=status.HTTP_400_BAD_REQUEST, data={"message": f"Param - {exc} missing"})
            except ValidationError as exc:
                return Response(status=status.HTTP_400_BAD_REQUEST, data={"message": f"{exc}"})
            except ValueError as exc:
                return Response(status=status.HTTP_400_BAD_REQUEST, data={"message": f"{exc}"})
            except Exception as exc:
                return Response(status=status.HTTP_400_BAD_REQUEST, data={"message": f"{exc}"})

        return decorator

    return wrapper
