from rest_framework import serializers
from apps.documents.models import Documents
from apps.invoices.serializers import InvoiceSerializer


class DocumentSerializer(serializers.ModelSerializer):
    invoice_details = serializers.SerializerMethodField()

    class Meta:
        model = Documents
        fields = ('id', "name", 'file', 'digitized_status', 'note', "invoice_details")

        read_only_fields = ('id', "invoice_details")


    def get_invoice_details(self, instance):
        # checking here if document is digitized then return invoice data
        if instance.digitized_status == Documents.COMPLETED:
            return InvoiceSerializer(instance.invoice).data
        else:
            return {}